This is libGE.info, produced by makeinfo version 6.5 from libGE.texi.

This manual is for libGE, version 0.28.0, a C++ library that implements
the Grammatical Evolution mapping process.

   Copyright (C) 2003-2006 Biocomputing-Developmental Systems Centre,
University of Limerick, Ireland.  Permission is granted to copy,
distribute and/or modify this document under the terms of the GNU Free
Documentation License, Version 1.2 or any later versions published by
the Free Software Foundation; with no Invariant Sections, no Front-Cover
Texts, and no Back-Cover Texts.  A copy of the license is included in
the section entitled "GNU Free Documentation License".


Indirect:
libGE.info-1: 662
libGE.info-2: 301289

Tag Table:
(Indirect)
Node: Top662
Node: Overview1861
Node: Purpose2060
Node: Installation4007
Node: Introduction to libGE6257
Node: Grammatical Evolution6528
Node: Example of Mapping Process12411
Node: Using libGE15399
Node: The Mapping Process17063
Node: The System Boundary19455
Node: Programming Interface20616
Node: Class Hierarchy21096
Node: Description27858
Node: Interfaces31564
Node: Genotype32564
Node: Phenotype37267
Node: Mapper40071
Node: Rule43984
Node: Production46289
Node: Symbol48720
Node: Grammar51058
Node: Tree54326
Node: CFGrammar57467
Node: GEGrammar62136
Node: Initialiser66155
Node: GEGrammarSI68446
Node: Designing your Own Mappers73692
Node: Grammars75285
Node: Format of Grammars76010
Node: Type of Grammars81642
Node: libGE Extensions87186
Node: Examples of Grammars89565
Node: Search Engines92381
Node: IlliGAL sga-c93125
Node: GALib97918
Node: EO102431
Node: Using your own Search Engine107606
Node: Evaluators108429
Node: GCC109325
Node: S-Lang113535
Node: TinyCC117690
Node: Lua121607
Node: Using your own Evaluator127393
Node: Examples127828
Node: Santa Fe Ant Trail Problem128487
Node: Santa Fe Grammar File129915
Node: Ant Trail130240
Node: Santa Fe Implementation with GE_ILLIGALSGA130547
Node: EXAMPLES/SantaFeAntTrail/GE_ILLIGALSGA/app.c131337
Node: EXAMPLES/SantaFeAntTrail/GE_ILLIGALSGA/Makefile132971
Node: Santa Fe Implementation with GE_MITGALIB133623
Node: GALib Example User Guide134930
Node: EXAMPLES/SantaFeAntTrail/GE_MITGALIB/main.cpp137297
Node: EXAMPLES/SantaFeAntTrail/GE_MITGALIB/initfunc.cpp140200
Node: EXAMPLES/SantaFeAntTrail/GE_MITGALIB/GEListGenome.h141396
Node: EXAMPLES/SantaFeAntTrail/GE_MITGALIB/santafe-gcc.cpp142470
Node: EXAMPLES/SantaFeAntTrail/GE_MITGALIB/santafe-slang.cpp144531
Node: EXAMPLES/SantaFeAntTrail/GE_MITGALIB/santafe-tcc.cpp146733
Node: EXAMPLES/SantaFeAntTrail/GE_MITGALIB/santafe-lua.cpp148900
Node: EXAMPLES/SantaFeAntTrail/GE_MITGALIB/Makefile151385
Node: Santa Fe Implementation with GE_EO152199
Node: EO Example User Guide153333
Node: EXAMPLES/SantaFeAntTrail/GE_EO/main.cpp155803
Node: EXAMPLES/SantaFeAntTrail/GE_EO/eoGE.h157820
Node: EXAMPLES/SantaFeAntTrail/GE_EO/eoGEInit.h158485
Node: EXAMPLES/SantaFeAntTrail/GE_EO/eoGEMutation.h159426
Node: EXAMPLES/SantaFeAntTrail/GE_EO/eoGEQuadCrossover.h159977
Node: EXAMPLES/SantaFeAntTrail/GE_EO/eoGEEvalFuncGCC.h160899
Node: EXAMPLES/SantaFeAntTrail/GE_EO/eoGEEvalFuncSlang.h162607
Node: EXAMPLES/SantaFeAntTrail/GE_EO/eoGEEvalFuncTCC.h164456
Node: EXAMPLES/SantaFeAntTrail/GE_EO/eoGEEvalFuncLua.h166306
Node: EXAMPLES/SantaFeAntTrail/GE_EO/Makefile168431
Node: Santa Fe Ant Trail Performance169454
Node: GALib performance on Santa Fe Ant Trail171166
Node: EO performance on Santa Fe Ant Trail171603
Node: Cart Centering Problem172028
Node: Cart Centering Optimal Solution176340
Node: Cart Centering Grammar177328
Node: EXAMPLES/CartCentering/GEcart.c178548
Node: EXAMPLES/CartCentering/cartcenterstart.c183058
Node: EXAMPLES/CartCentering/cartcenterend.c184248
Node: EXAMPLES/CartCentering/WrappedCPhenotype185259
Node: Cart Centering Implementation with GE_ILLIGALSGA186018
Node: EXAMPLES/CartCentering/GE_ILLIGALSGA/app.c186823
Node: EXAMPLES/CartCentering/GE_ILLIGALSGA/Makefile191165
Node: Cart Centering Implementation with GE_MITGALIB191824
Node: Cart Centering GALib Example User Guide193649
Node: EXAMPLES/CartCentering/GE_MITGALIB/main.cpp196194
Node: EXAMPLES/CartCentering/GE_MITGALIB/initfunc.cpp201751
Node: EXAMPLES/CartCentering/GE_MITGALIB/GEListGenome.h202931
Node: EXAMPLES/CartCentering/GE_MITGALIB/cartcenter-gcc.cpp204190
Node: EXAMPLES/CartCentering/GE_MITGALIB/cartcenter-slang.cpp209268
Node: EXAMPLES/CartCentering/GE_MITGALIB/GEcart.sl213645
Node: EXAMPLES/CartCentering/GE_MITGALIB/SLangStartCode214395
Node: EXAMPLES/CartCentering/GE_MITGALIB/SLangEndCode215706
Node: EXAMPLES/CartCentering/GE_MITGALIB/WrappedSlangPhenotype217049
Node: EXAMPLES/CartCentering/GE_MITGALIB/cartcenter-libtcc.cpp217745
Node: EXAMPLES/CartCentering/GE_MITGALIB/cartcenter-lua.cpp225207
Node: EXAMPLES/CartCentering/GE_MITGALIB/GEcart.lua230902
Node: EXAMPLES/CartCentering/GE_MITGALIB/LuaStartCode231642
Node: EXAMPLES/CartCentering/GE_MITGALIB/LuaEndCode232560
Node: EXAMPLES/CartCentering/GE_MITGALIB/WrappedLuaPhenotype233736
Node: EXAMPLES/CartCentering/GE_MITGALIB/Makefile234359
Node: Cart Centering Implementation with GE_EO235301
Node: Cart Centering EO Example User Guide236564
Node: EXAMPLES/CartCentering/GE_EO/main.cpp238809
Node: EXAMPLES/CartCentering/GE_EO/eoGE.h242532
Node: EXAMPLES/CartCentering/GE_EO/eoGEInit.h243256
Node: EXAMPLES/CartCentering/GE_EO/eoGEMutation.h244213
Node: EXAMPLES/CartCentering/GE_EO/eoGEQuadCrossover.h244772
Node: EXAMPLES/CartCentering/GE_EO/eoGEEvalFunc-gcc_tcc.h245720
Node: EXAMPLES/CartCentering/GE_EO/eoGEEvalFunc-slang.h250716
Node: EXAMPLES/CartCentering/GE_EO/eoGEEvalFunc-libtcc.h255092
Node: EXAMPLES/CartCentering/GE_EO/eoGEEvalFunc-lua.h262063
Node: EXAMPLES/CartCentering/GE_EO/Makefile267754
Node: Cart Centering Performance268902
Node: GALib performance on Cart Centering270508
Node: EO performance on Cart Centering270820
Node: Intertwined Spirals Problem271123
Node: Intertwined Spirals Grammar272416
Node: GEspiral.c273021
Node: spiralstart.c274929
Node: spiralend.c276242
Node: WrappedCPhenotype277059
Node: Intertwined Spirals Implementation with GE_MITGALIB277656
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/main.cpp281138
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/initfunc.cpp285431
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/GEListGenome.h286631
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/spiral-gcc.cpp287911
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/spiral-slang.cpp292784
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/GEspiral.sl297020
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/SLangStartCode297800
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/SLangEndCode299065
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/WrappedSLangPhenotype299923
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/spiral-libtcc.cpp301289
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/spiral-lua.cpp308127
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/GEspiral.lua313688
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/LuaStartCode314466
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/LuaEndCode315547
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/WrappedLuaPhenotype316439
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/Makefile317090
Node: Intertwined Spirals Implementation with GE_EO318044
Node: Intertwined Spirals EO Example User Guide319074
Node: EXAMPLES/IntertwinedSpirals/GE_EO/GEEA.cpp321388
Node: EXAMPLES/IntertwinedSpirals/GE_EO/eoGE.h325146
Node: EXAMPLES/IntertwinedSpirals/GE_EO/eoGEInit.h325890
Node: EXAMPLES/IntertwinedSpirals/GE_EO/eoGEMutation.h326867
Node: EXAMPLES/IntertwinedSpirals/GE_EO/eoGEQuadCrossover.h327446
Node: EXAMPLES/IntertwinedSpirals/GE_EO/eoGEEvalFunc-gcc_tcc.h328414
Node: EXAMPLES/IntertwinedSpirals/GE_EO/eoGEEvalFunc-slang.h333084
Node: EXAMPLES/IntertwinedSpirals/GE_EO/eoGEEvalFunc-libtcc.h337259
Node: EXAMPLES/IntertwinedSpirals/GE_EO/eoGEEvalFunc-lua.h343907
Node: EXAMPLES/IntertwinedSpirals/GE_EO/Makefile349314
Node: Intertwined Spirals Performance350479
Node: GALib performance on Intertwined Spirals352132
Node: EO performance on Intertwined Spirals352457
Node: FAQ352773
Node: Copying This Manual355372
Node: References376404
Node: Index377372

End Tag Table
