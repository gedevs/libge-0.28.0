//AGLookUp.h -*- C++ -*-
#ifndef _AGLOOKUP_H_
#define _AGLOOKUP_H_

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/* This is a class that implements the interface of a symbol look up table. When */
/* the BNF grammar is read in and the non-terminals and terminal symbols have to */
/* be matched with problem specific classes descending from AGSymbol, this       */
/* interface is used. A concerete, problem specific subclass of this class has to*/
/* be implemented for each problem.                                              */ 
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

#include "libGEdefs.h"

#include<iostream>
#include<string>
#include "AGSymbol.h"

using namespace std;

class AGLookUp{
	public:
                virtual AGSymbol* findAGSymbol(const Symbol&, const unsigned int productions=0, bool grammarMade=false);
		virtual ~AGLookUp();
};


#endif
