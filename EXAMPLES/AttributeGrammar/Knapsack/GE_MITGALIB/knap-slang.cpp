#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<exception>

#include<ga/ga.h>
#include<GE/ge.h>
#include "GEListGenome.h"

#include<slang.h>

using namespace std;


// GE mapper, declared in main.cpp
extern GEGrammarSI mapper;
extern int obj_calls;

// Fitness variable.
double fitness;     // training
double tFitness;    // test
int isInitialised;

//Initialises the mapper, and the s-lang interpreter
void app_init(unsigned int wrappingEvents, string
grammarFile){

  /* Init GE mapper */
  //Set maximum number of wrapping events per mapping
  mapper.setMaxWraps(wrappingEvents);
  
  //Load grammar
  if (!mapper.readBNFFile(grammarFile)){
    cerr << "Could not read "<< grammarFile << "\n";
    cerr << "Execution aborted.\n";
    exit(0);
  }

  /* Init S-lang interface */
  if (-1 == SLang_init_all () || (-1 == SLang_init_stdio ()) )
    exit (EXIT_FAILURE);

  if (-1 == SLang_load_file("GEknapsack.sl")){
    SLang_restart (1);
    SLang_set_error(0);
    cout << "-1 == SLang_load_file (\"GEknapsack.sl\"))";
  }
    
   string buffer=" value=initVals();";
    //Create interface with variable value
    if (-1 == SLadd_intrinsic_variable("value",
				     &(isInitialised), SLANG_INT_TYPE, 0))
    {
      exit(EXIT_FAILURE);
    }
  
  //Load and interpret buffer
    if (-1 == SLang_load_string(const_cast<char*>(buffer.c_str()))){
      SLang_restart(1);
      SLang_set_error(0);
    }
    
    if (isInitialised==-1)
    {
      cerr << "Error initialising data in GEsprial.sl. initVals() returned -1. Exiting ..";
      exit (0);    
    }
 
}


//Attributes a fitness score to a genome
float objfunc(GAGenome &g){
  obj_calls++;
  string buffer;
  GEListGenome &genome = static_cast<GEListGenome&>(g);

  fitness=tFitness=0;

/***********************************************************
* Sample debugging code to identify an offending genotype is
* given below. 
***********************************************************/
#if (DEBUG_LEVEL >= 1)
	cout << obj_calls << ": Before mapping genome" << endl;
	int ii=0;
		cout << "genome size" <<genome.size() <<endl;
                while(ii<genome.size()){
                       cout<< (int)(*genome.warp(ii++)) <<" , ";
                }
	cout<<endl;
#endif
	
     //Assign genotype to mapper
     mapper.setGenotype(genome);

     //Grab phenotype
     Phenotype const *phenotype=mapper.getPhenotype();

#if (DEBUG_LEVEL >= 1)
	cout << "Genotype = " << *mapper.getGenotype() << "\n";
	cout << *phenotype << endl;
#endif

    if(phenotype->getValid()){
    
    	//Write start code to buffer
	    // buffer="e=length(p_array)-1;\n"+phenotype->getString();
     	buffer=phenotype->getString();

    	//Write end code to buffer
    	buffer+=";\nFitness_Variable=AssignFitness(1)";

    	//Uncomment the following line if using out of sample test data. 
    	//buffer+="\n testFitness_Variable=AssignFitness(0);";
    
    	//Create interface with variable Fitness_Variable
    	if (-1 == SLadd_intrinsic_variable("Fitness_Variable",
				     &(fitness), SLANG_DOUBLE_TYPE, 0)){
      		exit(EXIT_FAILURE);
    	}
    	
    	//The following code is only useful if there is a separate test data.
    	//Create interface with variable Fitness_Variable
    	/*if (-1 == SLadd_intrinsic_variable("testFitness_Variable",
				     &(tFitness), SLANG_DOUBLE_TYPE, 0))
    		{
      		exit(EXIT_FAILURE);
    	}*/

    	//Load and interpret buffer
    	if (-1 == SLang_load_string(const_cast<char*>(buffer.c_str()))){
      		SLang_restart(1);
      		SLang_set_error(0);
      		cout << "obj_calls = " << obj_calls << "\n";
      		cout << "Genotype = " << *mapper.getGenotype() << "\n";
      		cout << "Effective length = " << mapper.getGenotype()->getEffectiveSize() << "\n";
      		cout << *phenotype << " (fitness = " << fitness << ")\n"; 
    	}
    	
    	if(isinf(fitness) || isnan(fitness) || !isfinite(fitness))
      		fitness = 0;
      
    	if(isinf(tFitness) || isnan(tFitness) || !isfinite(tFitness))
      		tFitness = 0;

    	// cout << *phenotype << " (" << fitness << ")\n"; 

    	// Set effective size of genome
    	genome.setEffectiveSize(mapper.getGenotype()->getEffectiveSize());
    	genome.setTestFitness(tFitness);

      return fitness;
  }
  return 0;
}


//Print an individual to stdout
void print_individual(const GAGenome &g){
  GAListGenome<unsigned char> &genome =
    (GAListGenome<unsigned char> &) g;
    
  //Assign genotype to mapper
  mapper.setGenotype(genome);
  cout << "Best individual:\n";
  
  //Print phenotype
  cout << *(mapper.getPhenotype());
  cout << endl;
  cout << "Genotype = " << *mapper.getGenotype() << "\n";
  cout << "Total length     = " << mapper.getGenotype()->size() << "\n";
  cout << "Effective length = " << mapper.getGenotype()->getEffectiveSize() << "\n";
  cout << "Wrapping events = " << mapper.getGenotype()->getWraps() << "\n";
}


void record_individual(const GAGenome & g, ofstream & st, unsigned int printGeno){

  try {
	  const GEListGenome &tempgen = dynamic_cast<const GEListGenome&>(g);
          GEListGenome genome;
          genome.copy(tempgen);
          objfunc(genome); 
 //         st << genome.getTestFitness() << " ";
  	  st << mapper.getGenotype()->size() << " ";
  	  st << mapper.getGenotype()->getEffectiveSize() << " ";
          if (printGeno) 
            st << *mapper.getGenotype();
          st << "\t" << *(mapper.getPhenotype());            
  
      } catch (exception & e) 
      {
    	  cout << "Exception: " << e.what();
          exit(0);
      }
  
}
